/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"HR_Management_Employees/HR_Management_Employees/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});