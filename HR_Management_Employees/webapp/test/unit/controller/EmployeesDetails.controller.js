/*global QUnit*/

sap.ui.define([
	"HR_Management_Employees/HR_Management_Employees/controller/EmployeesDetails.controller"
], function (Controller) {
	"use strict";

	QUnit.module("EmployeesDetails Controller");

	QUnit.test("I should test the EmployeesDetails controller", function (assert) {
		var oAppController = new Controller();
		oAppController.onInit();
		assert.ok(oAppController);
	});

});