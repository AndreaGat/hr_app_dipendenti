sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/Text",
	"sap/m/Button",
	"sap/m/Dialog",
	"sap/m/library",
	"sap/ui/model/json/JSONModel"
], function (Controller, Text, Button, Dialog, mobileLibrary, JSONModel) {
	"use strict";

	return Controller.extend("HR_Management_Employees.HR_Management_Employees.controller.EmployeesDetailsSecondView", {
		onInit: function () {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("EmployeesDetailsSecondView").attachMatched(this._onRouteMatched, this);
			this.setDatePickerMinDate();
		},
		_onRouteMatched: function (oEvent) {
			var idRichiesta = oEvent.getParameter("arguments").id;
			this.odataFunctionRead(idRichiesta);
		},
		odataFunctionRead: function (idRichiesta) {
			sap.ui.core.BusyIndicator.show(0);
			var that = this;
			var serviceUrl = "/HR_Management_EmployeesHR_Management_Employees/dbHR/odata/v2/CatalogService/";
			var modelColl = new sap.ui.model.odata.v2.ODataModel(serviceUrl);

			//Read entity
			modelColl.read("/FERIE_DIP(" + idRichiesta + ")", {
				async: true,
				urlParameters: {
					"$expand": "UTENTE"
				},
				success: function (oResult) {
					sap.ui.core.BusyIndicator.hide();
					var jsonModel = new JSONModel(oResult);

					that.getView().setModel(jsonModel, "ModelFerieDetails");
				},
				error: function (oError) {
					var dataModel = that.getView().getModel("ModelFerieDetails");
					if (dataModel) {
						dataModel.setData(null);
					}
					sap.ui.core.BusyIndicator.hide();
					var msg = "Attenzione: errore nella lettura dati";
					sap.m.MessageToast.show(msg);
				}
			});

		},
		odataFerieRead: function (employeeID) {
			var that = this;
			that.getView().setBusy(true);

			var serviceUrl = "/HR_Management_EmployeesHR_Management_Employees/dbHR/odata/v2/CatalogService/";
			var modelColl = new sap.ui.model.odata.v2.ODataModel(serviceUrl);

			//Filter
			var filters = [];
			//Apply filters to FERIE_DIP
			var filterID = new sap.ui.model.Filter("UTENTE_ID", sap.ui.model.FilterOperator.EQ, employeeID);
			filters.push(filterID);
			//read only FERIE_DIP linked to current user ID
			modelColl.read("/FERIE_DIP", {
				filters: [filters],
				async: true,
				success: function (oResult) {
					that.getView().setBusy(false);
					var results = oResult.results;

					var jsonModel = new JSONModel({
						FERIE_DIP: results
					});
					//get owner component model
					var modelComponent = that.getOwnerComponent().getModel("ModelFerieComponent");
					//update owner component model
					modelComponent.setData(jsonModel);
				}
			});
		},
		navView1: function () {
			var dataModel = this.getView().getModel("ModelFerieDetails");
			if (dataModel) {
				dataModel.setData(null);
			}
			this.getOwnerComponent().getRouter().navTo("TargetEmployeesDetails");
		},
		onSave: function (oEvent) {
			var that = this;

			var dateFromValue = that.getView().byId("DP01").getValue();
			var dateToValue = that.getView().byId("DP02").getValue();
			var noteDip = that.getView().byId("NoteDip").getValue();
			var idRichiesta = that.getView().getModel("ModelFerieDetails").getData().ID_RICHIESTA;
			var employeeID = that.getView().getModel("ModelFerieDetails").getData().UTENTE_ID;

			//Get today's date
			var today = new Date();

			if (!dateFromValue || !dateToValue) {
				that.messageDialogPress("Warning", "Attenzione! Inserire un valore nei campi 'Data inizio' e 'Data fine'");
			} else {
				var serviceUrl = "/HR_Management_EmployeesHR_Management_Employees/dbHR/odata/v2/CatalogService/";
				var modelColl = new sap.ui.model.odata.v2.ODataModel(serviceUrl);

				var oDialog = new Dialog({
					title: "Confirm",
					type: "Message",
					content: new Text({
						text: "Modificare giorni di ferie (dal " + dateFromValue + " al " + dateToValue + ")?"
					}),
					beginButton: new Button({
						type: mobileLibrary.ButtonType.Emphasized,
						text: "Conferma",
						press: function () {
							var newGgFerie = that.calcBusinessDays(dateFromValue, dateToValue);

							var entityFerieToUpdate = {
								DATA_INIZIO: dateFromValue + "T00:00:00",
								DATA_FINE: dateToValue + "T00:00:00",
								GG_FERIE: newGgFerie.toString(),
								DATA_RICHIESTA: today,
								STATUS: "I",
								NOTE_DIPENDENTE: noteDip.toString(),
								NOTE_RESPONSABILE: null,
								DATA_MODIFICA_STATUS: null
							};

							modelColl.update("/FERIE_DIP(" + idRichiesta + ")", entityFerieToUpdate, {
								success: function (data) {
									//TODO update FERIE_INIZIALI field
									// //Update FERIE_INIZIALI in Anagrafica entity
									// var ggFerieCurrent = employeeModel[0].FERIE_INIZIALI;
									// var entityAnagraficaUpdated = {
									// 	FERIE_INIZIALI: (ggFerieCurrent - newGgFerie).toString()
									// };
									// // read ANAGRAFICA_DIP filtered by employeeID
									// modelColl.update("/ANAGRAFICA_DIP('" + employeeID + "')", entityAnagraficaUpdated, {
									// 	success: function (data) {
									// 		that.odataEmployeesRead();
									// 	}
									// });
									that.odataFunctionRead(idRichiesta);
									that.messageDialogPress("Success", "Data ferie modificate");
									that.odataFerieRead(employeeID);
								},
								error: function (e) {
									that.messageDialogPress("Error", "Errore imprevisto nell'eliminazione delle ferie");
								}
							});

							oDialog.close();
						}
					}),
					endButton: new Button({
						text: "Cancel",
						press: function () {
							oDialog.close();
							that.messageDialogPress("Information", "Operazione annullata");
						}
					}),
					afterClose: function () {
						oDialog.destroy();
						that.setButtonVisibility(false);
						that.getView().byId("DP01").setValue(null);
						that.getView().byId("DP02").setValue(null);
					}
				});
				oDialog.open();
			}
		},
		onModify: function () {
			this.setButtonVisibility(true);
		},
		onCancel: function () {
			var that = this;
			that.setButtonVisibility(false);
			that.setDatePickerMinDate();
			that.getView().byId("DP01").setValue(null);
			that.getView().byId("DP02").setValue(null);
		},
		buttonVisibleFormatter: function (parameter) {
			if (parameter === "I") {
				return true;
			} else {
				return false;
			}
		},
		setButtonVisibility: function (bEdit) {
			var oView = this.getView();
			// Show the appropriate action buttons

			oView.byId("buttonModificaRichiesta").setVisible(!bEdit);
			oView.byId("panelModificaRichiesta").setVisible(bEdit);
			oView.byId("buttonSalvaModifica").setVisible(bEdit);
			oView.byId("buttonAnnullaModifica").setVisible(bEdit);
		},
		messageDialogPress: function (dialogState, contentText) {
			var oDialog = new Dialog({
				title: dialogState,
				type: "Message",
				state: dialogState,
				content: new Text({
					text: contentText
				}),
				beginButton: new Button({
					type: mobileLibrary.ButtonType.Emphasized,
					text: "OK",
					press: function () {
						oDialog.close();
					}
				}),
				afterClose: function () {
					oDialog.destroy();
				}
			});

			oDialog.open();
		},
		setDatePickerMinDate: function () {
			this.getView().byId("DP01").setMinDate(new Date());
			this.getView().byId("DP02").setEnabled(false);
		},
		datePickerChange: function (oControlEvent) {

			var dateFromValue = this.getView().byId("DP01").getValue();

			if (dateFromValue !== null) {

				this.getView().byId("DP02").setEnabled(true);
				this.getView().byId("DP02").setValue(null);
				this.getView().byId("DP02").setMinDate(new Date(dateFromValue));
			}
		},
		calcBusinessDays: function (dDate1, dDate2) {
			var date1 = new Date(dDate1);
			var date2 = new Date(dDate2);

			var iWeeks, iDateDiff, iAdjust = 0;

			var iWeekday1 = date1.getDay(); // day of week
			var iWeekday2 = date2.getDay();

			iWeekday1 = (iWeekday1 === 0) ? 7 : iWeekday1; // change Sunday from 0 to 7
			iWeekday2 = (iWeekday2 === 0) ? 7 : iWeekday2;

			if ((iWeekday1 > 5) && (iWeekday2 > 5)) {
				iAdjust = 1;
			} // adjustment if both days on weekend
			iWeekday1 = (iWeekday1 > 5) ? 5 : iWeekday1; // only count weekdays
			iWeekday2 = (iWeekday2 > 5) ? 5 : iWeekday2;

			// calculate difference in weeks (1000mS * 60sec * 60min * 24hrs * 7 days = 604800000)
			iWeeks = Math.floor((date2.getTime() - date1.getTime()) / 604800000);

			if (iWeekday1 <= iWeekday2) { //Equal to makes it reduce 5 days
				iDateDiff = (iWeeks * 5) + (iWeekday2 - iWeekday1);
			} else {
				iDateDiff = ((iWeeks + 1) * 5) - (iWeekday1 - iWeekday2);
			}
			iDateDiff -= iAdjust; // take into account both days on weekend

			return (iDateDiff + 1); // add 1 because dates are inclusive
		}
	});
});