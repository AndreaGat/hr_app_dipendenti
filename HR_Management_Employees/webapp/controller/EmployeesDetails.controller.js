sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/Text",
	"sap/m/Button",
	"sap/m/Dialog",
	"sap/m/library",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageToast",
	"sap/ui/export/library",
	"sap/ui/export/Spreadsheet"
], function (Controller, Text, Button, Dialog, mobileLibrary, JSONModel, MessageToast, library, Spreadsheet) {
	"use strict";

	return Controller.extend("HR_Management_Employees.HR_Management_Employees.controller.EmployeesDetails", {
		onInit: function () {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("TargetEmployeesDetails").attachMatched(this._onRouteMatched, this);

			this.odataEmployeesRead();
			this.countFerieID();
			this.setDatePickerMinDate();
		},
		_onRouteMatched: function (oEvent) {
			var modelComponent = this.getOwnerComponent().getModel("ModelFerieComponent");
			if (typeof modelComponent !== "undefined") {

				this.getView().setModel(modelComponent.getData(), "ModelFerie");

			}
		},
		//function to read ANAGRAFICA_DIP filtered by current logged user (we need this to get the ID to filter table FERIE_DIP by UTENTE_ID)
		odataEmployeesRead: function () {
			var that = this;
			that.getView().setBusy(true);
			var serviceUrl = "/HR_Management_EmployeesHR_Management_Employees/dbHR/odata/v2/CatalogService/";
			var modelColl = new sap.ui.model.odata.v2.ODataModel(serviceUrl);

			// Get cur user firstName and lastName
			var serviceUserInfo = sap.ushell.Container.getService("UserInfo").getUser();
			if (typeof serviceUserInfo !== "undefined") {
				var userFirstName = serviceUserInfo.getFirstName();
				var userLastName = serviceUserInfo.getLastName();
			} else {
				userFirstName = "Andrea";
				userLastName = "Gattulli";
			}

			//Filter
			var filters = [];
			//Apply filters to FERIE_DIP
			var filterFirstName = new sap.ui.model.Filter("NOME", sap.ui.model.FilterOperator.EQ, userFirstName);
			filters.push(filterFirstName);
			var filterLastName = new sap.ui.model.Filter("COGNOME", sap.ui.model.FilterOperator.EQ, userLastName);
			filters.push(filterLastName);

			modelColl.read("/ANAGRAFICA_DIP", {
				filters: [filters],
				async: true,
				success: function (oResult) {
					var results = oResult.results;

					var jsonModel = new JSONModel({
						ANAGRAFICA_DIP: results
					});
					that.getView().setModel(jsonModel, "Employees");

					that.odataFerieRead();
					that.getView().setBusy(false);
				},
				error: function (oError) {
					that.getView().setBusy(false);
					var msg = "Attenzione errore";
					MessageToast.show(msg);
				}
			});
		},
		//function to read FERIE_DIP odata filtered on UTENTE_ID current value
		odataFerieRead: function () {
			var that = this;
			that.getView().setBusy(true);

			var serviceUrl = "/HR_Management_EmployeesHR_Management_Employees/dbHR/odata/v2/CatalogService/";
			var modelColl = new sap.ui.model.odata.v2.ODataModel(serviceUrl);

			//Get Employees model to make a filter on FERIE_DIP
			var employeeModel = that.getView().getModel("Employees").getData().ANAGRAFICA_DIP;
			//Get the cur user ID
			var employeeID = employeeModel[0].ID;
			//Filter
			var filters = [];
			//Apply filters to FERIE_DIP
			var filterID = new sap.ui.model.Filter("UTENTE_ID", sap.ui.model.FilterOperator.EQ, employeeID);
			filters.push(filterID);
			//read only FERIE_DIP linked to current user ID
			modelColl.read("/FERIE_DIP", {
				filters: [filters],
				async: true,
				success: function (oResult) {
					that.getView().setBusy(false);
					var results = oResult.results;

					var jsonModel = new JSONModel({
						FERIE_DIP: results
					});
					//Set view model
					that.getView().setModel(jsonModel, "ModelFerie");

					//Set owner component model (to update values between pages)
					that.getOwnerComponent().setModel(jsonModel, "ModelFerieComponent");
				},
				error: function (oError) {
					that.getView().setBusy(false);
					var msg = "Attenzione errore";
					MessageToast.show(msg);
				}
			});
		},
		//function to create new FERIE_DIP record
		onSend: function (oEvent) {
			var that = this;

			var dateFromValue = that.getView().byId("DP01").getValue();
			var dateToValue = that.getView().byId("DP02").getValue();
			var noteDip = that.getView().byId("NoteDip").getValue();

			var today = new Date();

			if (!dateFromValue || !dateToValue) {
				that.messageDialogPress("Warning", "Attenzione! Inserire un valore nei campi 'Data inizio' e 'Data fine'");
			} else {
				var serviceUrl = "/HR_Management_EmployeesHR_Management_Employees/dbHR/odata/v2/CatalogService/";
				var modelColl = new sap.ui.model.odata.v2.ODataModel(serviceUrl);
				//Get Employees model to make a filter on FERIE_DIP
				var employeeModel = that.getView().getModel("Employees").getData().ANAGRAFICA_DIP;
				//Get the cur user ID
				var employeeID = employeeModel[0].ID;
				//READ model 
				var jsonModelCountID = that.getView().getModel("jsonModelCount").getData().count;
				var position = jsonModelCountID.length - 1;
				var newRequestId = jsonModelCountID[position].ID_RICHIESTA + 1;

				var oDialog = new Dialog({
					title: "Confirm",
					type: "Message",
					content: new Text({
						text: "Inserire giorni di ferie (dal " + dateFromValue + " al " + dateToValue + ")?"
					}),
					beginButton: new Button({
						type: mobileLibrary.ButtonType.Emphasized,
						text: "Conferma",
						press: function () {
							var newGgFerie = that.calcBusinessDays(dateFromValue, dateToValue);

							var entityFerie = {
								ID_RICHIESTA: newRequestId,
								DATA_INIZIO: dateFromValue + "T00:00:00", //add the final string just to save data on dB
								DATA_FINE: dateToValue + "T00:00:00",
								GG_FERIE: newGgFerie.toString(),
								DATA_RICHIESTA: today,
								STATUS: "I",
								NOTE_DIPENDENTE: noteDip.toString(),
								UTENTE_ID: employeeID
							};

							modelColl.create("/FERIE_DIP", entityFerie, {
								success: function () {

									//Update FERIE_INIZIALI in Anagrafica entity
									var ggFerieCurrent = employeeModel[0].FERIE_INIZIALI;
									var entityAnagraficaUpdated = {
										FERIE_INIZIALI: (ggFerieCurrent - newGgFerie).toString()
									};
									// read ANAGRAFICA_DIP filtered by employeeID
									modelColl.update("/ANAGRAFICA_DIP('" + employeeID + "')", entityAnagraficaUpdated, {
										success: function (data) {
											that.odataEmployeesRead();
										}
									});
									that.messageDialogPress("Success", "Inseriti " + newGgFerie + " giorni di ferie");
									that.countFerieID();
								},
								error: function (e) {
									that.messageDialogPress("Error", "Errore in fase di inserimento ferie");
								}
							});

							oDialog.close();
						}
					}),
					endButton: new Button({
						text: "Cancel",
						press: function () {
							oDialog.close();
							that.messageDialogPress("Information", "Operazione annullata");
						}
					}),
					afterClose: function () {
						oDialog.destroy();
						that.setButtonVisibility(false);
						that.getView().byId("DP01").setValue(null);
						that.getView().byId("DP02").setValue(null);
						that.getView().byId("NoteDip").setValue(null);
					}
				});
				oDialog.open();
			}
		},
		//function to delete selected FERIE_DIP records (1..n)
		onDelete: function (oEvent) {
			var that = this;

			var oTable = that.getView().byId("hrTable");
			var aContexts = oTable.getSelectedContexts();

			if (aContexts.length === 0) {
				that.messageDialogPress("Warning", "Attenzione! Selezionare almeno un record");
			} else {
				var oDialog = new Dialog({
					title: "Confirm",
					type: "Message",
					content: new Text({
						text: "Eliminare i record selezionati?"
					}),
					beginButton: new Button({
						type: mobileLibrary.ButtonType.Emphasized,
						text: "Conferma",
						press: function () {
							/*delete operation*/
							var serviceUrl = "/HR_Management_EmployeesHR_Management_Employees/dbHR/odata/v2/CatalogService/";
							var modelColl = new sap.ui.model.odata.v2.ODataModel(serviceUrl);

							for (var i = aContexts.length - 1; i >= 0; i--) {
								var idRichiesta = aContexts[i].getObject().ID_RICHIESTA;
								// var modelName = aContexts[i].getObject().modelName;
								// console.log(modelColl.getData());

								modelColl.remove("/FERIE_DIP(" + idRichiesta + ")", {
									success: function (data) {
										that.odataFerieRead();
										that.countFerieID();
									},
									error: function (e) {
										that.messageDialogPress("Error", "Errore imprevisto nell'eliminazione delle ferie");
									}
								});
							}
							that.messageDialogPress("Success", "Eliminate ferie");
							oDialog.close();
						}
					}),
					endButton: new Button({
						text: "Cancel",
						press: function () {
							oDialog.close();
							that.messageDialogPress("Information", "Operazione annullata");
						}
					}),
					afterClose: function () {
						oDialog.destroy();
					}
				});
				oDialog.open();
			}
		},
		//function called by clicking button id 'buttonNuovaRichiesta'
		onInsertNew: function () {
			this.setButtonVisibility(true);
		},
		//function called by clicking button id 'buttonAnnullaModifica'
		onCancel: function () {
			var that = this;
			that.setButtonVisibility(false);
			//clear datepicker's values
			that.getView().byId("DP01").setValue(null);
			that.getView().byId("DP02").setValue(null);
			that.getView().byId("NoteDip").setValue(null);
		},
		//function to count ALL record on FERIE_DIP table. We need this to get the last ID value (this doesn't break primary Key's table)
		countFerieID: function () {
			var that = this;
			var serviceUrl = "/HR_Management_EmployeesHR_Management_Employees/dbHR/odata/v2/CatalogService/";
			var modelColl = new sap.ui.model.odata.v2.ODataModel(serviceUrl);

			that.getView().setBusy(true);
			modelColl.read("/FERIE_DIP", {
				async: true,
				success: function (oResult) {
					var results = oResult.results;
					var jsonModelCount = new JSONModel({
						count: results
					});
					that.getView().setModel(jsonModelCount, "jsonModelCount");
				},
				error: function (oError) {
					that.getView().setBusy(false);
				}
			});
		},
		setButtonVisibility: function (bEdit) {
			var oView = this.getView();
			oView.byId("buttonNuovaRichiesta").setVisible(!bEdit);
			oView.byId("panelNuovaRichiesta").setVisible(bEdit);
			oView.byId("buttonSalvaNuova").setVisible(bEdit);
			oView.byId("buttonAnnullaModifica").setVisible(bEdit);
			oView.byId("buttonEliminaRichiesta").setEnabled(!bEdit);
		},
		messageDialogPress: function (dialogState, contentText) {
			var oDialog = new Dialog({
				title: dialogState,
				type: "Message",
				state: dialogState,
				content: new Text({
					text: contentText
				}),
				beginButton: new Button({
					type: mobileLibrary.ButtonType.Emphasized,
					text: "OK",
					press: function () {
						oDialog.close();
					}
				}),
				afterClose: function () {
					oDialog.destroy();
				}
			});

			oDialog.open();
		},
		//formatter button
		buttonHighlightFormatter: function (parameter) {
			var value;
			switch (parameter) {
			case "I":
				value = "Information";
				break;
			case "A":
				value = "Success";
				break;
			case "R":
				value = "Error";
				break;
			default:
				value = "None";
			}
			return value;
		},
		//set today's date as min. value in DatePicker DP01
		setDatePickerMinDate: function () {
			this.getView().byId("DP01").setMinDate(new Date());
			this.getView().byId("DP02").setEnabled(false);
		},
		//function called by changing DP01 DatePicker
		datePickerChange: function (oControlEvent) {

			var dateFromValue = this.getView().byId("DP01").getValue();

			if (dateFromValue !== null) {

				this.getView().byId("DP02").setEnabled(true);
				this.getView().byId("DP02").setValue(null);
				this.getView().byId("DP02").setMinDate(new Date(dateFromValue));
			}
		},
		//Method to calculate ONLY workDay (it excludes weekend)
		calcBusinessDays: function (dDate1, dDate2) {
			var date1 = new Date(dDate1);
			var date2 = new Date(dDate2);

			var iWeeks, iDateDiff, iAdjust = 0;

			var iWeekday1 = date1.getDay(); // day of week
			var iWeekday2 = date2.getDay();

			iWeekday1 = (iWeekday1 === 0) ? 7 : iWeekday1; // change Sunday from 0 to 7
			iWeekday2 = (iWeekday2 === 0) ? 7 : iWeekday2;

			if ((iWeekday1 > 5) && (iWeekday2 > 5)) {
				iAdjust = 1;
			} // adjustment if both days on weekend
			iWeekday1 = (iWeekday1 > 5) ? 5 : iWeekday1; // only count weekdays
			iWeekday2 = (iWeekday2 > 5) ? 5 : iWeekday2;

			// calculate difference in weeks (1000mS * 60sec * 60min * 24hrs * 7 days = 604800000)
			iWeeks = Math.floor((date2.getTime() - date1.getTime()) / 604800000);

			if (iWeekday1 <= iWeekday2) { //Equal to makes it reduce 5 days
				iDateDiff = (iWeeks * 5) + (iWeekday2 - iWeekday1);
			} else {
				iDateDiff = ((iWeeks + 1) * 5) - (iWeekday1 - iWeekday2);
			}

			iDateDiff -= iAdjust; // take into account both days on weekend

			return (iDateDiff + 1); // add 1 because dates are inclusive
		},
		//Nav to EmployeesDetailsSecondView by clicking one table's record
		onNavEmployeesDetailsSecondView: function (oEvent) {
			var oItem = oEvent.getSource();
			var oCtx = oItem.getBindingContext("ModelFerie");
			var idRichiesta = oCtx.getProperty("ID_RICHIESTA");

			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("EmployeesDetailsSecondView", {
				id: idRichiesta
			});
			this.onCancel();
		},
		// Function to export on CSV
		//add "sap/ui/export/library" and "sap/ui/export/Spreadsheet" in the definition
		onExport: function () {
			var EdmType = library.EdmType;

			var oSpreadsheet = new Spreadsheet({
				dataSource: this.getView().getModel("ModelFerie").getProperty("/FERIE_DIP"),
				fileName: "Ferie dipendente.xlsx",
				workbook: {
					columns: [{
						label: "Id richiesta",
						property: "ID_RICHIESTA",
						type: EdmType.Number
					}, {
						label: "Id utente",
						property: "UTENTE_ID",
						type: EdmType.String
					}, {
						label: "Data inizio",
						property: "DATA_INIZIO",
						type: EdmType.Date,
						width: 15
					}, {
						label: "Data fine",
						property: "DATA_FINE",
						type: EdmType.Date,
						width: 15
					}, {
						label: "Giorni ferie",
						property: "GG_FERIE",
						type: EdmType.Number,
						width: 5
					}, {
						label: "Data richiesta",
						property: "DATA_RICHIESTA",
						type: EdmType.Date,
						width: 15
					}, {
						label: "Stato richiesta",
						property: "STATUS",
						type: EdmType.String,
						width: 5
					}]
				}
			});

			oSpreadsheet.build().finally(function () {
				oSpreadsheet.destroy();
			});
		}
	});
});